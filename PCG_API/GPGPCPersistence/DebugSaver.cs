﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.Persistence
{
    class DebugSaver
    {
        public static StringBuilder InformationsBuilder
        {
            get;
            set;
        }

        public static void addInfo(string info)
        {
            if (InformationsBuilder == null)
                InformationsBuilder = new StringBuilder();
            InformationsBuilder.Append(info);
        }

        public static void saveInfo(string Path, string Name, bool Append)
        {
            System.IO.StreamWriter file =
                new System.IO.StreamWriter(Path + "\\" + Name, Append);
            file.WriteLine(InformationsBuilder.ToString());

            file.Close();
            InformationsBuilder.Clear();
        }
    }
}

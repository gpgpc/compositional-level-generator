﻿using GPGPC.AI.EvolutionaryAlgorithms;
using System;
using System.Collections.Generic;

namespace GPGPC.PCG.LevelCreation
{
    /// <summary>
    /// This class is a world generator. It maps artifacts (features, represented by symbols/strings)
    /// to functions (agents responsible for generating each of feature).
    /// </summary>
    /// <typeparam name="V">Artifact representation type. This is the type of the object returned by each agent function.</typeparam>
    /// <typeparam name="T">Type of the world repersentation.</typeparam>
    public class WorldGenerator<V, T>
    {
        /// <summary>
        /// Adds an artifact to the WorldGenerator.
        /// </summary>
        /// <param name="ArtifactKey">Artifact symbol.</param>
        /// <param name="ArtifactCreator">Agent function.</param>
        /// <returns>If the addition was successfull or not.</returns>
        public bool AddArtifact(string ArtifactKey, Func<T, V> ArtifactCreator)
        {
            try
            {
                if (Artifacts == null)
                {
                    Artifacts = new Dictionary<string, Func<T, V>>();
                }

                if (!Artifacts.ContainsKey(ArtifactKey))
                {
                    Artifacts.Add(ArtifactKey, ArtifactCreator);
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }

            Console.WriteLine("The key {0} already exists, try again", ArtifactKey);
            return false;
        }

        /// <summary>
        /// This method is responsable for the world generation. It iterates over a string
        /// of symbols generated by a grammar and uses the mapped agents whenever it encounters
        /// a terminal symbol that is mapped.
        /// </summary>
        /// <param name="Word">Word containing one or many artifacts.</param>
        /// <param name="WorldRep">Reference to a instance of the world representation where the artifacts will be created.</param>
        /// <returns>Returns the World Representation reference, altered by the agents.</returns>
        public T GenerateWorld(string Word, ref T WorldRep)
        {
            int i = 0;
            while (i < Word.Length)
            {
                string key = Grammar<string>.GetTerminal(Word, ref i);
                if (key != null)
                {
                    if (Artifacts.ContainsKey(key))
                        Artifacts[key](WorldRep);
                }
            }

            return WorldRep;
        }

        /// <summary>
        /// Dictionary that maps the terminal symbols to its agents.
        /// </summary>
        public Dictionary<string, Func<T, V>> Artifacts;
    }
}
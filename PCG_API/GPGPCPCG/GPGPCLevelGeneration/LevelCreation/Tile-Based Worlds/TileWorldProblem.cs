﻿using GPGPC.AI.SearchProblems;
using GPGPC.Utils;
using System.Collections.Generic;

namespace GPGPC.PCG.LevelCreation
{
    internal class TileWordProblem : IProblem<Vector2>
    {
        /// <summary>
        /// Constructor whichi initializes the world representation, the initial state
        /// and sets the goal state position.
        /// </summary>
        /// <param name="WrRep">Problem's world representation.</param>
        /// <param name="InitState">Problem's initial state.</param>
        /// <param name="Goal">Problem's goal state.</param>
        public TileWordProblem(TileWorld WrRep, Vector2 InitState, Vector2 Goal)
        {
            SetWorldRep(WrRep);
            SetInicialState(InitState);
            SetGoal(Goal);
        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public TileWordProblem()
        { }

        /// <summary>
        /// This method calculates the heuristic value from a state. It uses the
        /// manhattan distance between two positions as the heuristic.
        /// </summary>
        /// <param name="CurrentState">The current problem's state.</param>
        /// <param name="Successor">The successor problem's state.</param>
        /// <returns>The heuristic value between two states.</returns>
        public double Heuristic(State<Vector2> CurrentState, State<Vector2> Successor)
        {
            double h = Vector2.ManhattanDistance(CurrentState.GetValue(), Successor.GetValue());
            return h;
        }

        /// <summary>
        /// This method sets the problem's initial state.
        /// </summary>
        /// <param name="InitState"></param>
        public void SetInicialState(Vector2 InitState)
        {
            InicialState = new State<Vector2>(InitState);
        }

        /// <summary>
        /// This method returns the initial state of this problem.
        /// </summary>
        /// <returns>This problem's initial state.</returns>
        public State<Vector2> GetInitialState()
        {
            return InicialState;
        }

        /// <summary>
        /// This method returns all successors state, given a state. It checks the
        /// actions Up, Right, Down and Left. It considers only actions which leads to
        /// non traversables state as not legals.
        /// </summary>
        /// <param name="Current">State which will generate the successors.</param>
        /// <returns>List of successors triples.</returns>
        public List<Successor<Vector2>> Expand(State<Vector2> Current)
        {
            List<Successor<Vector2>> successors = new List<Successor<Vector2>>();

            for (int row = (int)Current.GetValue().X - 1; row <= Current.GetValue().X + 1; row += 2)
            {
                if (!(row < 0 || row > WorldRep.Length() - 1))
                {
                    string tile = WorldRep.GetValue(row, (int)Current.GetValue().Y);
                    if (!WorldRep.IsNonTraversable(tile))
                    {
                        Successor<Vector2> successorTriple;
                        successorTriple.state = new Vector2(row, Current.GetValue().Y);
                        successorTriple.action = "";
                        successorTriple.cost = 1;
                        successors.Add(successorTriple);
                    }
                }
            }

            for (int colum = (int)Current.GetValue().Y - 1; colum <= Current.GetValue().Y + 1; colum += 2)
            {
                if (!(colum < 0 || colum > WorldRep.Length() - 1))
                {
                    string tile = WorldRep.GetValue((int)Current.GetValue().X, colum);
                    if (!WorldRep.IsNonTraversable(tile))
                    {
                        Successor<Vector2> successorTriple;
                        successorTriple.state = new Vector2(Current.GetValue().X, colum);
                        successorTriple.action = "";
                        successorTriple.cost = 1;
                        successors.Add(successorTriple);
                    }
                }
            }
            return successors;
        }

        /// <summary>
        /// This method checks if the current state is a goal state. It compares
        /// the state position with the goal state position.
        /// </summary>
        /// <param name="CurrentState">State that will be checked.</param>
        /// <returns>If is or not the problem's goal state.</returns>
        public bool IsGoalState(State<Vector2> CurrentState)
        {
            return CurrentState.GetValue().Equals(Goal);
        }

        /// <summary>
        /// This method gets the goal state position.
        /// </summary>
        /// <returns>Position of the goal state.</returns>
        public Vector2 GetGoal()
        {
            return Goal;
        }

        /// <summary>
        /// Sets the goal state position.
        /// </summary>
        /// <param name="Goal">Position of the goal state.</param>
        public void SetGoal(Vector2 Goal)
        {
            this.Goal = Goal;
        }

        /// <summary>
        /// Sets the world representation of the problem.
        /// </summary>
        /// <param name="WrRep">Problem's world representation.</param>
        public void SetWorldRep(TileWorld WrRep)
        {
            WorldRep = WrRep;
        }

        /// <summary>
        /// Holds the problem's world representation.
        /// </summary>
        private TileWorld WorldRep;

        /// <summary>
        /// Will hold the initial state of the search.
        /// </summary>
        private State<Vector2> InicialState;

        /// <summary>
        /// Holds the position of the goal state.
        /// </summary>
        private Vector2 Goal;

        # region Strategies search
        /// <summary>
        /// Private strategy which defines a DFS search for this problem.
        /// </summary>
        public static Searchs<Vector2>.Strategy DFS = delegate(IProblem<Vector2> Problem, State<Vector2> Current, State<Vector2> Successor)
            {
                int layer = (int) Current.GetCost() - 1;
                Successor.SetCost(layer);
                Successor.SetParent(Current);
                return layer;
            };

        /// <summary>
        /// Private strategy which defines a BFS search for this problem.
        /// </summary>
        public static Searchs<Vector2>.Strategy BFS = delegate(IProblem<Vector2> Problem, State<Vector2> Current, State<Vector2> Successor)
            {
                int layer = (int) Current.GetCost() + 1;
                Successor.SetCost(layer);
                Successor.SetParent(Current);
                return layer;
            };

        /// <summary>
        /// Private strategy which defines a Greedy search for this problem.
        /// </summary>
        public static Searchs<Vector2>.Strategy Greedy = delegate(IProblem<Vector2> Problem, State<Vector2> Current, State<Vector2> Successor)
            {
                double heuristic = Problem.Heuristic(Current, Successor)*3;
                Successor.SetCost(Current.GetCost() + 1);
                Successor.SetParent(Current);
                return heuristic;
            };
        #endregion
    }
}
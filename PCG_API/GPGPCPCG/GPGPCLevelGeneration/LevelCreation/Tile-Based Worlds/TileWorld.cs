﻿using GPGPC.AI.SearchProblems;
using GPGPC.Utils;
using System;
using System.Collections.Generic;

namespace GPGPC.PCG.LevelCreation
{
    /// <summary>
    /// This class is a representation of a tile world. It implements a 2D world with strings as the
    /// representation of the artifacts (features) on it. It is a quadratic world and uses the
    /// "##" simbol as default/floor.
    /// </summary>
    internal class TileWorld : IWorld2D<string>
    {
        /// <summary>
        /// This constructor initializes the world with (Size x Size) width and height. The default simbol
        /// on the world is "##" and it is initialized on the whole map representation.
        /// </summary>
        /// <param name="Size">Size of the quadratic map.</param>
        public TileWorld(int Size)
        {
            NonTraversables = new List<string>();
            Interactables = new List<string>();
            Artifacts = new Dictionary<string, List<Vector2>>();
            Below = new Dictionary<string, string>();
            Map = new string[Size, Size];

            for (int row = 0; row < Length(); row++)
            {
                for (int colum = 0; colum < Length(); colum++)
                {
                    Map[row, colum] = "##";
                }
            }
        }

        /// <summary>
        /// Basic constructor which initializes the world as a 10x10 map.
        /// </summary>
        public TileWorld()
            : this(10)
        {
            
        }

        /// <summary>
        /// This method sets a value into a X and Y position on the map.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <param name="Value">String value to be set on the map.</param>
        public void SetValue(int X, int Y, string Value)
        {
            Map[X, Y] = Value;
        }

        /// <summary>
        /// Function that returns a value, given a position on the map.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <returns>A string value contained on the map.</returns>
        public string GetValue(int X, int Y)
        {
            return Map[X, Y];
        }

        /// <summary>
        /// The size of a quadratic map.
        /// </summary>
        /// <returns></returns>
        public int Length()
        {
            //Console.WriteLine("WARNING - Using length works correctly only if your tile map has the same width and heigth");
            return Map.GetUpperBound(0) + 1;
        }

        /// <summary>
        /// This method sets the current map representation as the representation
        /// contained on the parametrer map.
        /// </summary>
        /// <param name="Map">Map representation as a string matrix</param>
        public void SetMap(string[,] Map)
        {
            for (int row = 0; row < Length(); row++)
            {
                for (int colum = 0; colum < Length(); colum++)
                {
                    this.Map[row, colum] = Map[row, colum];
                }
            }
        }

        /// <summary>
        /// This method return a matrix representing the 2 dimensions world 
        /// containing the simbols that represents the artifacts featuring
        /// the world.
        /// </summary>
        /// <returns>The low structure map representation as a matrix os strings.</returns>
        public string[,] GetMap()
        {
            return Map;
        }

        /// <summary>
        /// Inserts a simbol as a non-traversable tile.
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        public bool AddNonTraversable(string Simbol)
        {
            try
            {
                if (!NonTraversables.Contains(Simbol))
                    NonTraversables.Add(Simbol);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the simbol is a non-traversable.
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a non traversable, false otherwise.</returns>
        public bool IsNonTraversable(string Simbol)
        {
            return NonTraversables.Contains(Simbol);
        }

        /// <summary>
        /// List of non traversables simbols on the map.
        /// </summary>
        /// <returns>A list containin all non traversables.</returns>
        public List<string> GetNonTraversables()
        {
            return NonTraversables;
        }

        /// <summary>
        /// Inserts a simbol as a interactable object to be placed over of the tilemap.
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        public bool AddInteractable(string Simbol)
        {
            try
            {
                if (!Interactables.Contains(Simbol))
                    Interactables.Add(Simbol);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if the simbol is a interactable.
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a interactable, false otherwise.</returns>
        public bool IsInteractable(string Simbol)
        {
            return Interactables.Contains(Simbol);
        }

        /// <summary>
        /// List of the interactables contained on the map.
        /// </summary>
        /// <returns>A list with the interactables on the map.</returns>
        public List<string> GetInteractables()
        {
            return Interactables;
        }

        /// <summary>
        /// Inserts a simbol as a artifact (game element).
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        public bool AddArtifact(string Simbol, Vector2 Position)
        {
            try
            {
                if (!containsArtifact(Simbol))
                    Artifacts.Add(Simbol, new List<Vector2>());
                Artifacts[Simbol].Add(Position);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// This method checks if a simbol is a artifact (feature).
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a artifact, false otherwise.</returns>
        public bool containsArtifact(string Simbol)
        {
            return Artifacts.ContainsKey(Simbol);
        }

        /// <summary>
        /// This method gets all the position in which a simbol is found.
        /// </summary>
        /// <param name="Simbol">Simbol which the position will be seek.</param>
        /// <returns>A list of positions.</returns>
        public List<Vector2> GetArtifactPositions(string Simbol)
        {
            return Artifacts[Simbol];
        }

        /// <summary>
        /// This method gets a randomly chosen position from Simbol which 
        /// features the world currently.
        /// </summary>
        /// <param name="Simbol">Simbol which the position will be seek.</param>
        /// <returns>A randomly chosen position from Simbol on the world.</returns>
        public Vector2 GetArtifactPositionAtRandom(String Simbol)
        {
            return Artifacts[Simbol][PRNG.Next(Artifacts[Simbol].Count)];
        }

        /// <summary>
        /// Dictionary that maps a interactable to a simbol which will be placed below it.
        /// </summary>
        public Dictionary<string, string> Below
        {
            get;
            set;
        }

        /// <summary>
        /// This dictionary maps artifacts keys into a list of position that
        /// exists on the world.
        /// </summary>
        private Dictionary<string, List<Vector2>> Artifacts;

        /// <summary>
        /// Lists which holds the Non Traversables and Interactables that features the world.
        /// </summary>
        private List<string> NonTraversables, Interactables;

        /// <summary>
        /// Matrix which holds the representation of the world.
        /// </summary>
        private string[,] Map;

        /// <summary>
        /// Pseudo Random Number Generator.
        /// </summary>
        private static Random PRNG = new Random();
    }
}
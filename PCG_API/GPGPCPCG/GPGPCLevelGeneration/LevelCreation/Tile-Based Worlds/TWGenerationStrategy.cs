﻿using GPGPC.AI.SearchProblems;
using GPGPC.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GPGPC.PCG.LevelCreation
{
    /// <summary>
    /// This is a abstract class which is a model for Strategies that will be used for the level generations.
    /// </summary>
    public abstract class TWGenerationStrategy
    {
        /// <summary>
        /// Pseudo Random Number Generator.
        /// </summary>
        protected static Random PRNG = new Random();

        /// <summary>
        /// Constructor which sets the map size. It is not possible to create a strategy that
        /// has no size especification.
        /// </summary>
        /// <param name="p_MapSize">Size of a quadratic map.</param>
        public TWGenerationStrategy(int p_MapSize)
        {
            this.MapSize = p_MapSize;
        }

        /// <summary>
        /// Property which holds the quadratic map size;
        /// </summary>
        public int MapSize { get; set; }

        /// <summary>
        /// It is required to override this method to get the strategy configuration.
        /// A strategy configuration should map an artifact/feature, which will be represented
        /// with a string (terminal symbol), to a function that perceives the environment and creates
        /// the artifact on the map (agent).
        /// </summary>
        /// <returns>A dictionary mapping terminal symbols to agent functions.</returns>
        public abstract Dictionary<string, Func<IWorld2D<string>, TileRepresentation>> BuildStrategyConfiguration();


        #region Helper Functions



        /// <summary>
        /// Helper method. Creats a wall with a specified direction and initial position
        /// from horizontal borders.
        /// </summary>
        /// <param name="direction">Direction the wall will grow.</param>
        /// <param name="TileMap">Tile map where the artifact will be created.</param>
        /// <param name="Size">Size of the wall.</param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="Artifact"></param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        protected virtual void BuildHorizontalLine(int direction, IWorld2D<string> TileMap, int Size,
            string Simbol, ref TileRepresentation Artifact, bool IsNonTraversable = true, string Below = null)
        {
            const int E = 1;
            int startPos;
            int row = PRNG.Next(1, TileMap.Length() - 1);

            if (direction == E)
                startPos = TileMap.Length() - 1;
            else
                startPos = 0;

            while (Size-- > 0)
            {
                CreateTileAt(TileMap, Simbol, ref Artifact, row, startPos, IsNonTraversable, Below);

                if (direction == E)
                    startPos--;
                else
                    startPos++;
            }
        }

        /// <summary>
        /// Helper method. Creats a wall with a specified direction and initial position.
        /// from vertical borders.
        /// </summary>
        /// <param name="direction">Direction the wall will grow.</param>
        /// <param name="TileMap">Tile map where the artifact will be created.</param>
        /// <param name="Size">Size of the wall.</param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="Artifact"></param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        protected virtual void BuildVerticalLine(int direction, IWorld2D<string> TileMap, int Size,
            string Simbol, ref TileRepresentation Artifact, bool IsNonTraversable = true, string Below = null)
        {
            const int N = 0;
            int startPos;
            int colum = PRNG.Next(1, TileMap.Length() - 1);

            if (direction == N)
                startPos = 0;
            else
                startPos = TileMap.Length() - 1;

            while (Size-- > 0)
            {
                CreateTileAt(TileMap, Simbol, ref Artifact, startPos, colum, IsNonTraversable, Below);

                if (direction == N)
                    startPos++;
                else
                    startPos--;
            }
        }

        /// <summary>
        /// Creates an artifact at the X/Y coordinates on the tilemap.
        /// This method will not create the artifact in case the X/Y coordinate
        /// already has an artifact other than the default simbol ("##").
        /// This rule does not apply for the initial ("IS") and goal ("GS") state.
        /// </summary>
        /// <param name="TileMap">Tile map where the artifact will be created.</param>
        /// <param name="Simbol">Simbol that represents the artifact on the map.</param>
        /// <param name="Artifact">Reference of the artifact being created.</param>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        /// <returns>If the creation was successful or not.</returns>
        protected bool CreateTileAt(IWorld2D<string> TileMap, string Simbol, ref TileRepresentation Artifact, int X, int Y, 
            bool IsNonTraversable, string Below = null)
        {
            // if the chosen tile has something and the simbol is not the Goal or Initial State, early out.
            if (TileMap.GetValue(X, Y) != "##" && !Simbol.Equals("ET") && !Simbol.Equals("EX"))
            {
                return false;
            }

            TileMap.SetValue(X, Y, Simbol);
            Artifact.Value = Simbol;
            Artifact.X = X;
            Artifact.Y = Y;

            TileMap.AddArtifact(Simbol, new Vector2(X, Y));

            if (IsNonTraversable)
                TileMap.AddNonTraversable(Simbol);

            if (Below != null)
            {
                TileMap.AddInteractable(Simbol);
                if (!TileMap.Below.ContainsKey(Simbol))
                    TileMap.Below.Add(Simbol, Below);
            }

            return true;
        }

        /// <summary>
        /// Creates an artifact at the tilemap at a random coordinates.
        /// This method will not create the artifact if the randomized coordinate
        /// already has an artifact other than the default simbol ("##").
        /// This rule does not apply for the initial ("IS") and goal ("GS") state.
        /// </summary>
        /// <param name="TileMap">Tile map where the artifact will be created.</param>
        /// <param name="ThisArtifact">Simbol that represents the artifact on the map.</param>
        /// <returns>If the creation was successful or not.</returns>
        protected bool CreateTileAtRandom(IWorld2D<string> TileMap, string ThisArtifact)
        {
            int row = PRNG.Next(TileMap.Length());
            int colum = PRNG.Next(TileMap.Length());
            TileRepresentation artifact = new TileRepresentation();
            return CreateTileAt(TileMap, ThisArtifact, ref artifact, row, colum, false);
        }

        #endregion

        #region Agent Generators

        //////////////////////////////////////////////////// Sandbox Functions ////////////////////////////////////////////////////
        /// This functions can be used to generate agent functions that will act in a generalized way.                        /////
        /// This agents are: RandomPlacementAgent, GroupPlacementAgent and ProximityBasedAgent.                               /////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// This function generates a method that creates artifact arround another one. It performs a graph search
        /// from the position of one of 'ThisArtifact' and generates the artifact in the expect distance
        /// from it with a probability of generatin closer than expected.
        /// </summary>
        /// <param name="ThisArtifact">Simbol that represents the artifact that will be seek to be the pivot.</param>
        /// <param name="ExpectDistance">Expected distance to Simbol be generated</param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        /// <param name="Threshold">The probability of generating on the expected distance.</param>
        /// <returns>A function that creates a artifact arround another one at the passed tile map.</returns>
        protected virtual Func<IWorld2D<string>, TileRepresentation> GenerateProximityBasedAgent(string ThisArtifact, int ExpectDistance, string Simbol,
                    bool IsNonTraversable = false, string Below = null, float Threshold = 0.8f)
        {
            return delegate(IWorld2D<string> TileMap)
            {
                TileRepresentation artifact = new TileRepresentation();
                if (!TileMap.containsArtifact(ThisArtifact))
                {
                    CreateTileAtRandom(TileMap, ThisArtifact);
                }

                List<Vector2> contenders = TileMap.GetArtifactPositions(ThisArtifact);
                Vector2 choosenPosition = contenders[PRNG.Next(contenders.Count)];
                TileWorld AsTileWorld = (TileWorld)TileMap;
                TileWordProblem problem = new TileWordProblem(AsTileWorld, choosenPosition, null);
                List<Vector2> possiblePositions = new List<Vector2>();

                List<State<Vector2>> fringe = new List<State<Vector2>>();
                List<Vector2> closed = new List<Vector2>();
                State<Vector2> currentState = problem.GetInitialState();
                fringe.Add(currentState);
                int count = 0;

                while (count < ExpectDistance)
                {
                    currentState = fringe.Last();
                    List<Successor<Vector2>> successors = problem.Expand(currentState);
                    while (successors.Count > 0)
                    {
                        int choosenIndex = PRNG.Next(successors.Count);
                        Successor<Vector2> successor = successors[choosenIndex];
                        Vector2 successorPosition = successor.state;
                        if (!closed.Contains(successorPosition))
                        {
                            closed.Add(successorPosition);
                            fringe.Add(new State<Vector2>(successor.state));
                        }
                        possiblePositions.Add(successorPosition);
                        successors.RemoveAt(choosenIndex);
                    }

                    count++;
                }

                double prob = PRNG.NextDouble();
                if (possiblePositions.Count < 1)
                    return artifact;

                choosenPosition = possiblePositions.Last();

                if (prob > Threshold)
                {
                    choosenPosition = possiblePositions[PRNG.Next(possiblePositions.Count)];
                }

                int row = (int)choosenPosition.X;
                int colum = (int)choosenPosition.Y;
                CreateTileAt(TileMap, Simbol, ref artifact, row, colum, IsNonTraversable, Below);
                return artifact;
            };
        }

        /// <summary>
        /// This method creates a function which generates a block in a tile map. It generates the block
        /// with a probability of not one of the tiles does not be created. The initial position is choosen
        /// by a PRNG. Amount/2 tiles of the block are generate per row and column.
        /// </summary>
        /// <param name="Amount">Number of tiles </param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        /// <param name="Threshold">The probability of generatiing the expected with the expect amount.</param>
        /// <returns>A function that generates a block at the passed tile map.</returns>
        protected virtual Func<IWorld2D<string>, TileRepresentation> GenerateGroupPlacementAgent(int Amount, string Simbol,
                    bool IsNonTraversable = true, string Below = null, float Threshold = 0.7f)
        {
            return delegate(IWorld2D<string> TileMap)
            {
                TileRepresentation artifact = new TileRepresentation();
                int row = PRNG.Next(1, TileMap.Length() - 1);
                int column = PRNG.Next(1, TileMap.Length() - 1);

                for (int i = row; i < row + (Amount / 2); i++)
                {
                    for (int j = column; j < column + (Amount / 2); j++)
                    {
                        try
                        {
                            if (PRNG.NextDouble() < Threshold)
                            {
                                CreateTileAt(TileMap, Simbol, ref artifact, i, j, IsNonTraversable, Below);
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            //Console.WriteLine(e.StackTrace);
                        }
                    }
                }
                return artifact;
            };
        }

        /// <summary>
        /// This method generates a function that places a generated artifact 
        /// randomly in a tile map, following a uniform distribution.
        /// </summary>
        /// <param name="Maximum">Maximum number of artifacts expected to be created.</param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is interactable, this is the simbol which will be below it.</param>
        /// <returns>A function that places a artifact following a uniform distribution.</returns>
        protected virtual Func<IWorld2D<string>, TileRepresentation> GenerateRandomPlacementAgent(int Maximum, string Simbol, 
            bool IsNonTraversable = false, string Below = null)
        {
            return delegate(IWorld2D<string> TileMap)
            {
                TileRepresentation artifact = new TileRepresentation();
                int N = Maximum;

                while (N-- > 0)
                {
                    int row = PRNG.Next(TileMap.Length());
                    int column = PRNG.Next(TileMap.Length());
                    CreateTileAt(TileMap, Simbol, ref artifact, row, column, IsNonTraversable, Below);
                }
                return artifact;
            };
        }



        /// <summary>
        /// This method generates a function that randomly choses one border
        /// of the tile map and places a set of <paramref name="Simbol"/> starting there and following a direction.
        /// It considers the directions as "North = 0, East = 1, South = 2, West = 3".
        /// The generated function acts as an agent on the tile map.        
        /// </summary>
        /// <param name="MaxSize">The maximum size of the generated wall.</param>
        /// <param name="Simbol">Simbol that represents the artifact to be generated on the map.</param>
        /// <param name="IsNonTraversable">If the simbol is a non traversable one.</param>
        /// <param name="Below">If the simbol is an interactable and should be placed on top of the tile map,
        /// this is the simbol which will be below it.</param>
        /// <returns>A function that generates grows a set of <paramref name="Simbol"/> from a border of the map.</returns>
        protected virtual Func<IWorld2D<string>, TileRepresentation> GenerateLinePlacementAgent(int MaxSize, string Simbol,
            bool IsNonTraversable = true, string Below = null)
        {
            const int N = 0, E = 1, S = 2, W = 3;

            return delegate(IWorld2D<string> TileMap)
            {
                TileRepresentation artifact = new TileRepresentation();
                int direction = PRNG.Next(4);
                int Size = PRNG.Next(MaxSize);

                switch (direction)
                {
                    case N:
                        BuildVerticalLine(direction, TileMap, Size, Simbol, ref artifact, IsNonTraversable, Below);
                        break;

                    case E:
                        BuildHorizontalLine(direction, TileMap, Size, Simbol, ref artifact, IsNonTraversable, Below);
                        break;

                    case S:
                        BuildVerticalLine(direction, TileMap, Size, Simbol, ref artifact, IsNonTraversable, Below);
                        break;

                    case W:
                        BuildHorizontalLine(direction, TileMap, Size, Simbol, ref artifact, IsNonTraversable, Below);
                        break;
                }
                return artifact;
            };
        }
        #endregion
    }
}
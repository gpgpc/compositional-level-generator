﻿using GPGPC.AI.EvolutionaryAlgorithms;
using GPGPC.PCG.LevelCreation;
using GPGPC.PCG.LevelTypes;
using GPGPC.Utils;
using System;

//using System.Threading.Tasks;

namespace GPGPC
{
    public enum LevelGenerationType
    {
        LG_MAZE = 0,
        ROGUE = 1,
        EXAMPLE_ROGUE = 2
    }

    /// <summary>
    /// Struct which holds the required parametrers to the procedural level generation.
    /// </summary>
    public struct PLGAParametrers
    {
        
        public int GenotypeSize, GenotypeMaxRepetitions, PopulationSize, TournamentSize, MaxGenerations, Range;

        public float RecombinationFactor, MutationFactor, Threshold;

        /// <summary>
        /// Constructor that initializes all parametrers.
        /// </summary>
        /// <param name="GenotypeSize">Size of the genotype.</param>
        /// <param name="MaxGenotypeRepetitions">Maximum numbers a genotype might be reused on a mapping process.</param>
        /// <param name="PopulationSize">Size of the population.</param>
        /// <param name="TournamentSize">Size of the tournament.</param>
        /// <param name="RecombinationFactor">Probability of crossover.</param>
        /// <param name="MutationFactor">Probability of mutation.</param>
        /// <param name="MaxGenerations">Maximum number of generation.</param>
        /// <param name="Threshold">Intended fitness of the solution.</param>
        /// <param name="Range">Range of the values on the genotype.</param>
        public PLGAParametrers(int GenotypeSize, int MaxGenotypeRepetitions, int PopulationSize, int TournamentSize,
            float RecombinationFactor, float MutationFactor, int MaxGenerations, float Threshold, int Range)
        {
            this.GenotypeSize = GenotypeSize;
            this.GenotypeMaxRepetitions = MaxGenotypeRepetitions;
            this.PopulationSize = PopulationSize;
            this.TournamentSize = TournamentSize;
            this.RecombinationFactor = RecombinationFactor;
            this.MutationFactor = MutationFactor;
            this.MaxGenerations = MaxGenerations;
            this.Threshold = Threshold;
            this.Range = Range;
        }
    }

    /// <summary>
    /// A base class to all levels generators.
    /// </summary>
    /// <typeparam name="V">Artifact representation type. This is the type of the object returned by each agent function.</typeparam>
    /// <typeparam name="T">Type of the world repersentation.</typeparam>
    public abstract class ProceduralLevelGenerator<V, T>
    {
        /// <summary>
        /// Pseudo Random Number Generator.
        /// </summary>
        protected static Random PRNG = new Random();

        /// <summary>
        /// Constructor which initializes the level generator.
        /// </summary>
        public ProceduralLevelGenerator()
        {
            //this.MapSize = Strategy.MapSize;
            this.Grammar = this.CreateGeneratorGrammar();
            //this.TWGenerationStrategy = Strategy;
            //this.WorldGenerator = new TWGenerator();
            //this.WorldGenerator.Artifacts = this.TWGenerationStrategy.BuildStrategyConfiguration();
            this.FitnessFunction = this.CreateFitnessFuction();
            this.Grammar.SetFitnessFunction(this.FitnessFunction);
        }

        /// <summary>
        /// Property which holds the fitness function of the level.
        /// </summary>
        protected EvolutionalGrammar.FitnessFunction FitnessFunction { get; set; }

        /// <summary>
        /// Property which holds the evolutional grammar of the level.
        /// </summary>
        protected EvolutionalGrammar Grammar { get; set; }

        /// <summary>
        /// Instance of a world generator.
        /// </summary>
        protected WorldGenerator<V, T> WorldGenerator { get; set; }

        /// <summary>
        /// Factory Method for all TileBased levels.
        /// </summary>
        /// <param name="Type">The type of the procedural generator.</param>
        /// <param name="p_Strategy">The TileWorld Generation Strategy which will be used on the level generator.</param>
        /// <param name="p_Params">Generation parametrers.</param>
        /// <returns>A procedural level generator.</returns>
        public static TWProceduralLevelGenerator CreateTWLevelGenerator(LevelGenerationType Type, TWGenerationStrategy p_Strategy, params object[] p_Params)
        {
            switch (Type)
            {
                case LevelGenerationType.EXAMPLE_ROGUE:
                    return (TWProceduralLevelGenerator) new GPGPC.PCG.LevelTypes.ExampleRogueLikeGenerator(p_Strategy, (int) p_Params[0]);
                default:
                    return null;
            }
        }

        /// <summary>
        /// This method generates a procedural level. It uses a grammatical evolution for the definition of
        /// which artifacts (features) the level will have, and software agent functions for their creation
        /// and positioning.
        /// </summary>
        /// <param name="Parametrers">A struct containing all parametrers for the level generation.</param>
        /// <returns>A procedurally generated level.</returns>
        public T PLGA(PLGAParametrers Parametrers)
        {
            int GenotypeSize = Parametrers.GenotypeSize;
            int GenotypeMaxRepetitions = Parametrers.GenotypeMaxRepetitions;
            int PopulationSize = Parametrers.PopulationSize;
            int TournamentSize = Parametrers.TournamentSize;
            int MaxGenerations = Parametrers.MaxGenerations;
            float RecombinationFactor = Parametrers.RecombinationFactor;
            float MutationFactor = Parametrers.MutationFactor;
            float Threshold = Parametrers.Threshold;
            int Range = Parametrers.Range;
            return PLGA(GenotypeSize, GenotypeMaxRepetitions, PopulationSize, TournamentSize,
            RecombinationFactor, MutationFactor, MaxGenerations, Threshold, Range);
        }

        /// <summary>
        /// This method generates a procedural level. It uses a grammatical evolution for the definition of
        /// which artifacts (features) the level will have, and software agent functions for their creation
        /// and positioning.
        /// </summary>
        /// <param name="GenotypeSize">Size of the genotype.</param>
        /// <param name="MaxGenotypeRepetitions">Maximum numbers a genotype might be reused on a mapping process.</param>
        /// <param name="PopulationSize">Size of the population.</param>
        /// <param name="TournamentSize">Size of the tournament.</param>
        /// <param name="RecombinationFactor">Probability of crossover.</param>
        /// <param name="MutationFactor">Probability of mutation.</param>
        /// <param name="MaxGenerations">Maximum number of generation.</param>
        /// <param name="Threshold">Intended fitness of the solution.</param>
        /// <param name="Range">Range of the values on the genotype.</param>
        /// <returns>A procedurally generated level.</returns>
        public T PLGA(int GenotypeSize, int MaxGenotypeRepetitions, int PopulationSize, int TournamentSize,
            float RecombinationFactor, float MutationFactor, int MaxGenerations, float Threshold, int Range)
        {
            while (true)
            {
                string generatedWord = this.Grammar.EvolutionaryAlgorithm(GenotypeSize, MaxGenotypeRepetitions,
                    PopulationSize, TournamentSize, RecombinationFactor, MutationFactor, MaxGenerations, Threshold, Range);
                T levelRep = this.TransformWordInLevel(generatedWord);
                if (IsLevelPlayable(levelRep))
                {
                    Console.WriteLine("\nWord generated:\n  {0}\n", generatedWord);
                    return levelRep;
                }
                    
            }
        }

        /// <summary>
        /// Template Method for the creation of the world representation.
        /// </summary>
        /// <param name="word">The EG generated word that will fuel the agents.</param>
        /// <returns>The Level Representation created from the word.</returns>
        protected abstract T TransformWordInLevel(string word);
        
        /// <summary>
        /// Template Method for the creation of the fitness function used to evaluate the grammar generated words.
        /// </summary>
        /// <returns>A fitness function for evolution grammar evalutation.</returns>
        protected abstract EvolutionalGrammar.FitnessFunction CreateFitnessFuction();

        /// <summary>
        /// Template Method that generates the evolutional grammar
        /// that will be used by the algorithm on the level generation.
        /// Here must be defined the symbols and the production rules of the EvolutionGrammar and return it.
        /// </summary>
        /// <returns>The created evolutional grammar.</returns>
        protected abstract EvolutionalGrammar CreateGeneratorGrammar();

        /// <summary>
        /// Template Method that should return if a path through the initial state 
        /// to the goal state of the map exists.
        /// </summary>
        /// <param name="Map">Map which will be evaluated.</param>
        /// <returns>If exist or not a path to the goal state.</returns>
        protected abstract bool IsLevelPlayable(T LevelRep);
    }
}
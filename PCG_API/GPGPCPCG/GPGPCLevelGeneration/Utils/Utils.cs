﻿using GPGPC.AI.SearchProblems;
using GPGPC.PCG.LevelCreation;
using GPGPC.PCG.Tests;
using GPGPC.Persistence;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPGPC.Utils
{
    internal class LevelGenarationUtilities
    {
        public static string MapsToCSVHeatMap(List<TileWorld> Maps, string Simbol)
        {
            int size = Maps[0].Length();
            int[,] counter = new int[size, size];
            // creates keys for all tiles positions
            foreach (TileWorld map in Maps)
            {
                size = map.Length();
                for (int i = 1; i < size; i++)
                {
                    for (int j = 1; j < size; j++)
                    {
                        counter[i, j] = 0;
                    }
                }
            }
            // counts the artifacts contained on each position for all maps.
            foreach (TileWorld map in Maps)
            {
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (map.GetValue(i, j).Equals(Simbol))
                        {
                            counter[i, j] += 1;
                        }
                    }
                }
            }
            // writes the csv file
            StringBuilder sb = new StringBuilder();
            // header
            for (int i = 0; i < size; i++)
            {
                sb.Append(i + "");
                if (i != (size - 1))
                    sb.Append(";");
            }
            sb.Append("\n");
            //
            for (int i = 0; i < size; i++)
            {
                sb.Append("Y"+ i + ";");
                for (int j = 0; j < size; j++)
                //for (int j = size-1; j >= 0; j--)
                {
                    sb.Append(counter[i,j] + "");
                    // the last has no ';'
                    if (j != (size - 1))
                        sb.Append(";");
                }
                if (i != (size - 1))
                    sb.Append("\n");
            }
            return sb.ToString();
        }

        public static string GetDirection(Vector2 Current, Vector2 Next)
        {
            if (Next.X > Current.X)
                return "Down";

            if (Next.X < Current.X)
                return "Up";

            if (Next.Y > Current.Y)
                return "Right";

            if (Next.Y < Current.Y)
                return "Left";

            return "Hit";
        }

        public static void DisplayActions(Queue<string> Actions)
        {
            foreach (string action in Actions)
                Console.Write(action + ", ");
            Console.WriteLine();
        }

        public static Queue<string> GetActions(List<State<Vector2>> Path)
        {
            Queue<string> actions = new Queue<string>();

            for (int i = 0; i < Path.Count - 1; i++)
            {
                Vector2 current = Path[i].GetValue();
                Vector2 next = Path[i + 1].GetValue();
                actions.Enqueue(LevelGenarationUtilities.GetDirection(current, next));
            }
            return actions;
        }

        public static string PrintPath(TileWorld TileMap, List<State<EnemyState>> Path)
        {
            List<State<Vector2>> path = new List<State<Vector2>>();

            foreach (State<EnemyState> state in Path)
            {
                State<Vector2> statePosition = new State<Vector2>(state.GetValue().GetPosition());
                path.Add(statePosition);
            }

            return LevelGenarationUtilities.PrintPath(TileMap, path);
        }

        public static string PrintPath(TileWorld TileMap, List<State<Vector2>> Path)
        {
            TileWorld World = new TileWorld(TileMap.Length());
            World.SetMap(TileMap.GetMap());

            for (int i = 0; i < Path.Count; i++)
            {
                Vector2 cordinates = Path[i].GetValue();
                //Console.WriteLine("Position {0}", cordinates);
                if (i < 9)
                    World.SetValue((int)cordinates.X, (int)cordinates.Y, "0" + (i + 1));
                else
                    World.SetValue((int)cordinates.X, (int)cordinates.Y, "" + (i + 1));
            }

            LevelGenarationUtilities.DisplayActions(GetActions(Path));
            return LevelGenarationUtilities.ToTileMap(World);
        }

        public static string ToTileMap(TileWorld TileMap)
        {
            string map = "";
            //map += "\n------ Tile Map ------\n";
            //Console.WriteLine("\n------ Tile Map ------\n");

            for (int row = 0; row < TileMap.Length(); row++)
            {
                for (int colum = 0; colum < TileMap.Length(); colum++)
                {
                    map += TileMap.GetValue(row, colum) + " ";
                    //Console.Write(TileMap.GetValue(row, colum)+" ");
                }
                map += "\n";
                //Console.WriteLine();
            }
            //map += "------ Size " + TileMap.Length() + " ------\n";
            //Console.WriteLine();
            //Console.WriteLine("------ Size {0} ------\n", TileMap.Length());
            //Console.WriteLine(map);
            return map;
        }
    }
}
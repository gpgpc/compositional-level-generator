﻿using System;
using System.Collections.Generic;

namespace GPGPC.Utils
{
    /// <summary>
    /// This class represents a vector in two dimensions. Its main responsability is represent
    /// a position of a artifact or agent in the map.
    /// </summary>
    public class Vector2
    {
        /// <summary>
        /// X and Y cordinates of the vector.
        /// </summary>
        private float x, y;

        /// <summary>
        /// Pseudo Random Number Generator.
        /// </summary>
        private static Random PRNG = new Random();

        /// <summary>
        /// Simple constructor which initializes the x and y cordinates with 0.
        /// </summary>
        public Vector2()
            : this(0f, 0f)
        {
        }

        /// <summary>
        /// Constructor which initializes the x and y cordinates.
        /// </summary>
        /// <param name="x">X cordinate value.</param>
        /// <param name="y">Y cordinate value.</param>
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// This method generates a list of vectors.
        /// </summary>
        /// <param name="N">Number of vectors will be generated.</param>
        /// <param name="MaxX">Maximum value of the x cordinate.</param>
        /// <param name="MaxY">Maximum value of the y cordinate.</param>
        /// <returns>A list of vectors.</returns>
        public static List<Vector2> GenerateRandomVectors(int N = 100, float MaxX = 1000, float MaxY = 1000)
        {
            List<Vector2> Vectors = new List<Vector2>(N);
            while (N-- > 0)
                Vectors.Add(new Vector2((float)(PRNG.NextDouble() * MaxX), (float)(PRNG.NextDouble() * MaxY)));
            return Vectors;
        }

        /// <summary>
        /// This method calculates the manhattan distance from two vectors.
        /// </summary>
        /// <param name="i">Vector i.</param>
        /// <param name="j">Vector j.</param>
        /// <returns>The distance between i and j.</returns>
        public static float ManhattanDistance(Vector2 CurrentState, Vector2 Successor)
        {
            return Math.Abs(CurrentState.X - Successor.X) + Math.Abs(CurrentState.Y - Successor.Y);
        }

        /// <summary>
        /// This method calculates the mean square distance from two vectors.
        /// </summary>
        /// <param name="i">Vector i.</param>
        /// <param name="j">Vector j.</param>
        /// <returns>The distance between i and j.</returns>
        public static float MeanSquareDistance(Vector2 i, Vector2 j)
        {
            float x = i.x - j.x;
            float y = i.y - j.y;
            return (float) Math.Sqrt(x * x + y * y);
        }

        /// <summary>
        /// This method sums two vectors.
        /// </summary>
        /// <param name="i">Vector a</param>
        /// <param name="j">Vector b</param>
        /// <returns>A new vector with the sum of a and b.</returns>
        public static Vector2 Sum(Vector2 i, Vector2 j)
        {
            return new Vector2(i.x + j.x, i.y + j.y);
        }

        /// <summary>
        /// Property which holds the x corinate.
        /// </summary>
        public float X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        /// <summary>
        /// Property which holds the y corinate.
        /// </summary>
        public float Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public override String ToString()
        {
            String resultado = "(" + x + "," + y + ")";
            return resultado;
        }

        public override bool Equals(Object obj)
        {
            Vector2 outro = (Vector2)obj;
            if (x != outro.x || y != outro.y)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
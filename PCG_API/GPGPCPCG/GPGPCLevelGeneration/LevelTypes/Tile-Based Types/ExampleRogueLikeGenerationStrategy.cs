﻿using GPGPC;
using GPGPC.PCG.LevelCreation;
using GPGPC.Utils;
using System;
using System.Collections.Generic;

public class ExampleRogueLikeGenerationStrategy : TWGenerationStrategy
{
    public ExampleRogueLikeGenerationStrategy(int p_MapSize)
        : base(p_MapSize)
    {
    }

    private Vector2 m_InitialStateLocation;

    public override Dictionary<string, Func<IWorld2D<string>, TileRepresentation>> BuildStrategyConfiguration()
    {
        Dictionary<string, Func<IWorld2D<string>, TileRepresentation>> simbol_mapping = new Dictionary<string, Func<IWorld2D<string>, TileRepresentation>>();

        simbol_mapping.Add("<entrance>", this.GenerateEntrancePlacingAgent("ET", 2));
        simbol_mapping.Add("<exit>", this.GenerateExitPlacingAgent("EX", MapSize * 2));
        simbol_mapping.Add("<chest>", this.GenerateRandomPlacementAgent(1, "CH", false, "##"));
        simbol_mapping.Add("<rock>", this.GenerateGroupPlacementAgent(4, "RK", true, null, 0.8f));
        simbol_mapping.Add("<enemy>", this.GenerateRandomPlacementAgent(1, "EE", false, "##"));

        return simbol_mapping;
    }

    private Func<IWorld2D<string>, TileRepresentation> GenerateEntrancePlacingAgent(string p_Simbol, int p_MaxDistanceFromWall)
    {
        Func<IWorld2D<string>, TileRepresentation> placement_function = delegate(IWorld2D<string> p_TileMap)
        {
            TileRepresentation artifact = new TileRepresentation();
            Vector2 location;
            // Verification variables
            bool away_from_left_wall, away_from_top_wall, away_from_right_wall, away_from_bottom_wall;

            do
            {
                location = Vector2.GenerateRandomVectors(1, p_TileMap.Length(), p_TileMap.Length())[0];

                away_from_left_wall = location.X - p_MaxDistanceFromWall >= 0;
                away_from_top_wall = location.Y - p_MaxDistanceFromWall >= 0;
                away_from_right_wall = location.X + p_MaxDistanceFromWall <= this.MapSize;
                away_from_bottom_wall = location.Y + p_MaxDistanceFromWall <= this.MapSize;

            } while (away_from_left_wall && away_from_top_wall && away_from_right_wall && away_from_bottom_wall);

            this.m_InitialStateLocation = location;
            this.CreateTileAt(p_TileMap, p_Simbol, ref artifact, (int) location.X, (int) location.Y, false);
            return artifact;
        };

        return placement_function;
    }

    private Func<IWorld2D<string>, TileRepresentation> GenerateExitPlacingAgent(string p_Simbol, int p_MinDistanceFromEntrance)
    {
        Func<IWorld2D<string>, TileRepresentation> placement_function = delegate(IWorld2D<string> p_TileMap)
        {
            float distance = float.MaxValue;
            Vector2 exit = null;
            int count = 0;

            do
            {
                exit = Vector2.GenerateRandomVectors(1, this.MapSize, this.MapSize)[0];
                distance = Vector2.ManhattanDistance(exit, this.m_InitialStateLocation);
                count++;

                if (count >= 50)
                {
                    count = 0;
                    p_MinDistanceFromEntrance--;
                }
            } while (distance < p_MinDistanceFromEntrance);

            TileRepresentation artifact = default(TileRepresentation);
            this.CreateTileAt(p_TileMap, p_Simbol, ref artifact, (int)exit.X, (int)exit.Y, false);
            return artifact;
        };

        return placement_function;
    }
}
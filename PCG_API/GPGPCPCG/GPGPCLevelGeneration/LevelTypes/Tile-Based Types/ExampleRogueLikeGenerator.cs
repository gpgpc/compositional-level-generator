﻿using GPGPC.AI.EvolutionaryAlgorithms;
using GPGPC.AI.SearchProblems;
using GPGPC.PCG.LevelCreation;
using GPGPC.Utils;
using System;
using System.Collections.Generic;

namespace GPGPC.PCG.LevelTypes
{
    public class ExampleRogueLikeGenerator : TWProceduralLevelGenerator
    {
        private static string ENTRANCE = "<entrance>",
            EXIT = "<exit>",
            CHEST = "<chest>",
            ROCK = "<rock>",
            ENEMY = "<enemy>";

        public ExampleRogueLikeGenerator(TWGenerationStrategy p_Strategy, int p_MaxChestCount)
            : base(p_Strategy)
        {
            this.MaxChestCount = p_MaxChestCount;
        }

        private int MaxChestCount { get; set; }

        protected override EvolutionalGrammar.FitnessFunction CreateFitnessFuction()
        {
            return delegate(string word, Grammar<string> grammar)
            {
                float fitness = 0;

                int MAX = this.MapSize * 2;

                int index = 0;
                int chest_count = 0;
                int rock_count = 0;
                while (index < word.Length)
                {
                    string simbol = Grammar<string>.GetTerminal(word, ref index);
                    if (simbol == null) continue;
                    if (char.IsUpper(simbol[1])) continue;

                    switch (simbol)
                    {
                        case "<chest>":
                            chest_count += 1;
                            break;

                        case "<rock>":
                            rock_count += 1;
                            break;

                        default:
                            break;
                    }
                }

                if (chest_count > this.MaxChestCount) return this.MaxChestCount - chest_count;

                if (chest_count <= 0) return -1;

                fitness += chest_count + rock_count;

                fitness *= 100;
                fitness /= MAX;
                fitness /= 100;

                if (fitness < 1)
                    return fitness;
                return -1;
            };
        }

        protected override EvolutionalGrammar CreateGeneratorGrammar()
        {
            EvolutionalGrammar grammar = new EvolutionalGrammar("<Start>", ENTRANCE + " <Rock> <Chest> " + EXIT);
            this.DefineChestRules(ref grammar);
            this.DefineRockRules(ref grammar);
            return grammar;
        }

        protected override bool IsLevelPlayable(IWorld2D<string> Map)
        {
            TileWorld map = (TileWorld)Map;
            Vector2 goalPosition = map.GetArtifactPositionAtRandom("ET");
            Vector2 initialPosition = map.GetArtifactPositionAtRandom("EX");
            State<Vector2> goal = new State<Vector2>(goalPosition);
            TileWordProblem pathProblem = new TileWordProblem();
            pathProblem.SetWorldRep(map);
            pathProblem.SetInicialState(initialPosition);
            pathProblem.SetGoal(goalPosition);
            List<State<Vector2>> path;
            path = Searchs<Vector2>.GraphSearch(pathProblem, null, goal);
            if (path == null)
                return false;
            string lowLevelStructure = GPGPC.Utils.LevelGenarationUtilities.ToTileMap(map);
            Console.WriteLine(lowLevelStructure);
            return true;
        }

        private void DefineChestRules(ref EvolutionalGrammar p_Grammar)
        {
            p_Grammar.AddProduction("<Chest>", "<Chest> " + CHEST + ENEMY + " <Rock> ");
            p_Grammar.AddProduction("<Chest>", "<Chest> " + CHEST + ENEMY + ENEMY);
            p_Grammar.AddProduction("<Chest>", "<Chest> " + CHEST + ENEMY);
            p_Grammar.AddProduction("<Chest>", CHEST);
            p_Grammar.AddProduction("<Chest>", " ");
        }

        private void DefineRockRules(ref EvolutionalGrammar p_Grammar)
        {
            p_Grammar.AddProduction("<Rock>", "<Rock> " + ROCK);
            p_Grammar.AddProduction("<Rock>", ROCK);
            p_Grammar.AddProduction("<Rock>", " ");
        }
    }
}
﻿using GPGPC.PCG.LevelCreation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC
{
    /// <summary>
    /// This struct represents a tile from a generated artifact.
    /// </summary>
    public struct TileRepresentation
    {
        public int X, Y;
        public string Value;

        /// <summary>
        /// Constructor which sets the position and value of a tile from a artifact.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <param name="Value">Value to be setted.</param>
        public TileRepresentation(int X = 5, int Y = 5, string Value = "#")
        {
            this.X = X;
            this.Y = Y;
            this.Value = Value;
        }

        public override string ToString()
        {
            string str = "Value: " + Value;
            str += " pos X: " + X;
            str += " pos Y: " + Y;
            return str;
        }
    }

    public abstract class TWProceduralLevelGenerator : ProceduralLevelGenerator<TileRepresentation, IWorld2D<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="TWStrategy">Strategy which will be used on the level generation</param></param>
        public TWProceduralLevelGenerator(TWGenerationStrategy TWStrategy)
            : base()
        {
            this.MapSize = TWStrategy.MapSize;
            this.TWGenerationStrategy = TWStrategy;
            this.WorldGenerator = new WorldGenerator<TileRepresentation, IWorld2D<string>>();
            this.WorldGenerator.Artifacts = this.TWGenerationStrategy.BuildStrategyConfiguration();
        }

        protected override IWorld2D<string> TransformWordInLevel(string word)
        {
            IWorld2D<string> levelRep = new TileWorld(this.MapSize);
            return this.WorldGenerator.GenerateWorld(word, ref levelRep);
        }

        /// <summary>
        /// Property which holds the strategy of the level.
        /// </summary>
        public TWGenerationStrategy TWGenerationStrategy { get; set; }
        
        /// <summary>
        /// Property which holds the size of the quadratic level.
        /// </summary>
        protected int MapSize { get; set; }
    }
}

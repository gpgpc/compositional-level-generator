﻿
using GPGPC.AI.EvolutionaryAlgorithms;
using GPGPC.AI.SearchProblems;
using GPGPC.PCG.LevelTypes;
using GPGPC.PCG.LevelCreation;
using GPGPC.PCG.Tests;
using GPGPC.Utils;
using System;
using System.Collections.Generic;

namespace GPGPC.PCG.Tests
{
    class TestFile
    {
        public static void Main(string[] args)
        {
            int rogueSize = 12;
            int GenotypeSize = rogueSize;
            int GenotypeMaxRepetitions = 3;
            int PopulationSize = 100;
            int TournamentSize = 5;
            int MaxGenerations = 100;
            float RecombinationFactor = 0.7f;
            float MutationFactor = 0.1f;
            float Threshold = 0.5f;
            int Range = 10;

            int MAX_CHESTS = 5;
            PLGAParametrers parametrers = new PLGAParametrers(GenotypeSize, GenotypeMaxRepetitions, PopulationSize,
                TournamentSize, RecombinationFactor, MutationFactor, MaxGenerations, Threshold, Range);
            TWProceduralLevelGenerator rogueMap = TWProceduralLevelGenerator.CreateTWLevelGenerator(LevelGenerationType.EXAMPLE_ROGUE,
                    new ExampleRogueLikeGenerationStrategy(rogueSize), MAX_CHESTS);

            List<TileWorld> maps = new List<TileWorld>();
            int count = 1;
            while (count-- > 0)
                maps.Add((TileWorld) rogueMap.PLGA(parametrers));
        }
    }
}

﻿using GPGPC.AI.SearchProblems;
using GPGPC.Utils;
using System.Collections.Generic;

// Change to list becouse the damn dictionary keeps fucking my life
namespace GPGPC.PCG.Tests
{
    internal class EnemysProblem : IProblem<EnemyState>
    {
        public EnemysProblem(EnemyTileWorld WrRep, EnemyState InitState)
        {
            SetWorldRep(WrRep);
            SetInicialState(InitState);
        }

        public EnemysProblem()
        { }

        public void SetInicialState(EnemyState InitState)
        {
            InicialState = new State<EnemyState>(InitState);
        }

        public State<EnemyState> GetInitialState()
        {
            return InicialState;
        }

        public bool IsGoalState(State<EnemyState> Current)
        {
            EnemyState current = Current.GetValue();
            //Console.WriteLine("Current state = position: {0}, num of enemys: {1}", current.GetPosition(), current.EnemysCount());
            //foreach (Vector2 enemy in current.GetEnemysPosition())
            //{
            //    Console.WriteLine(enemy);
            //}
            //return current.GetEnemysPosition().Contains(current.GetPosition());
            //Console.WriteLine("Contains enemys alive? {0}", current.ContainsEnemyAlive());
            //return current.EnemysCount() < 1;
            return !current.ContainsEnemyAlive();
        }

        public List<Successor<EnemyState>> Expand(State<EnemyState> Current)
        {
            //Console.WriteLine("Expanded Node: {0}", Current.GetValue());
            List<Successor<EnemyState>> successors = new List<Successor<EnemyState>>();
            Vector2 currentPos = Current.GetValue().GetPosition();

            for (int row = (int)Current.GetValue().GetPosition().X - 1; row <= Current.GetValue().GetPosition().X + 1; row += 2)
            {
                if (!(row < 0 || row > WorldRep.Length() - 1))
                {
                    string tile = WorldRep.GetValue(row, (int)Current.GetValue().GetPosition().Y);
                    if (!WorldRep.IsNonTraversable(tile))
                    {
                        Successor<EnemyState> successorTriple;
                        successorTriple.action = "";
                        successorTriple.cost = 1;

                        Vector2 nextPos = new Vector2(row, (int)Current.GetValue().GetPosition().Y);

                        EnemyState new_state = Current.GetValue().Copy();
                        new_state.SetPosition(nextPos);

                        //Console.WriteLine(tile);
                        if (WorldRep.IsInteractable(tile))
                        {
                            //Console.WriteLine("interageble found by: {0}", currentPos);
                            new_state.SetPosition(currentPos);
                            //Console.WriteLine("The nextPos is: {0}", nextPos);
                            new_state.SetEnemyStatus(nextPos, false);
                            //Console.WriteLine("Last created: {0}", new_state);
                        }
                        successorTriple.state = new_state;
                        successors.Add(successorTriple);
                    }
                }
            }

            for (int colum = (int)Current.GetValue().GetPosition().Y - 1; colum <= Current.GetValue().GetPosition().Y + 1; colum += 2)
            {
                if (!(colum < 0 || colum > WorldRep.Length() - 1))
                {
                    string tile = WorldRep.GetValue((int)Current.GetValue().GetPosition().X, colum);
                    if (!WorldRep.IsNonTraversable(tile))
                    {
                        Successor<EnemyState> successorTriple;
                        successorTriple.action = "";
                        successorTriple.cost = 1;

                        Vector2 nextPos = new Vector2((int)Current.GetValue().GetPosition().X, colum);

                        EnemyState new_state = Current.GetValue().Copy();
                        new_state.SetPosition(nextPos);
                        //Console.WriteLine(tile);

                        if (WorldRep.IsInteractable(tile))
                        {
                            //Console.WriteLine("interageble found by: {0}", currentPos);
                            new_state.SetPosition(currentPos);
                            //Console.WriteLine("The nextPos is: {0}", nextPos);
                            new_state.SetEnemyStatus(nextPos, false);
                            //Console.WriteLine("Last created: {0}", new_state);
                        }
                        successorTriple.state = new_state;
                        successors.Add(successorTriple);
                    }
                }
            }

            return successors;
        }

        public double Heuristic(State<EnemyState> Current, State<EnemyState> Goal)
        {
            //return Current.GetValue().EnemysCount();
            double heuristic = 0;
            EnemyState currentState = Current.GetValue();
            Vector2 position = currentState.GetPosition();

            foreach (Vector2 enemyPosition in currentState.GetEnemysPosition())
            {
                if (currentState.GetEnemyStatus(enemyPosition))
                {
                    double dist = Vector2.ManhattanDistance(position, enemyPosition);
                    heuristic += dist;
                }
            }

            heuristic += currentState.EnemysCount();
            return heuristic / 3;
        }

        public void SetWorldRep(EnemyTileWorld WrRep)
        {
            WorldRep = WrRep;
        }

        private EnemyTileWorld WorldRep;
        private State<EnemyState> InicialState;
    }
}
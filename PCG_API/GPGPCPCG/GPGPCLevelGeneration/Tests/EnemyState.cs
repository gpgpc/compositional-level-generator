﻿using GPGPC.Utils;
using System.Collections.Generic;

namespace GPGPC.PCG.Tests
{
    internal class EnemyState
    {
        public EnemyState(Vector2 Position, List<Vector2> EnemysPosition)
        {
            this.Position = Position;
            this.EnemysPosition = new List<Vector2>();
            this.EnemysStatus = new List<bool>();

            if (EnemysPosition == null)
                return;

            foreach (Vector2 position in EnemysPosition)
            {
                if (!this.EnemysPosition.Contains(position))
                    this.EnemysPosition.Add(position);
            }

            //Console.WriteLine("Position of the new state {0}", this.Position);
            foreach (Vector2 position in EnemysPosition)
            {
                EnemysStatus.Add(true);
            }
        }

        public EnemyState()
            : this(new Vector2(), new List<Vector2>())
        {
            //
        }

        public EnemyState Copy()
        {
            EnemyState new_state = new EnemyState();
            new_state.SetPosition(Position);
            new_state.SetEnemysPosition(EnemysPosition);
            new_state.SetEnemysStatus(EnemysStatus);
            return new_state;
        }

        public int EnemysCount()
        {
            int count = 0;
            foreach (bool enemy in EnemysStatus)
            {
                if (enemy)
                    count++;
            }
            return count;
        }

        public bool ContainsEnemyAlive()
        {
            foreach (bool enemy in EnemysStatus)
            {
                if (enemy)
                    return true;
            }
            return false;
        }

        public Vector2 GetPosition()
        {
            return Position;
        }

        public void SetPosition(Vector2 Position)
        {
            this.Position = Position;
        }

        public List<Vector2> GetEnemysPosition()
        {
            return EnemysPosition;
        }

        public void SetEnemysPosition(List<Vector2> EnemysPosition)
        {
            this.EnemysPosition = new List<Vector2>();
            this.EnemysPosition.AddRange(EnemysPosition);
        }

        public List<bool> GetEnemysStatus()
        {
            return EnemysStatus;
        }

        public void SetEnemysStatus(List<bool> EnemysStatus)
        {
            this.EnemysStatus = new List<bool>();
            this.EnemysStatus.AddRange(EnemysStatus);
        }

        public void SetEnemyStatus(Vector2 Position, bool Status)
        {
            int index = EnemysPosition.IndexOf(Position);
            EnemysStatus[index] = false;
            //Console.WriteLine("enemy {0} dead!", Position);
        }

        public bool ContainsEnemy(Vector2 Enemy)
        {
            return EnemysPosition.Contains(Enemy);
        }

        public bool GetEnemyStatus(Vector2 Position)
        {
            int index = EnemysPosition.IndexOf(Position);

            if (index >= 0)
                return EnemysStatus[index];

            return false;
        }

        public bool IsEnemyAlive(Vector2 EnemyPosition)
        {
            return GetEnemyStatus(EnemyPosition);
        }

        public override bool Equals(object obj)
        {
            EnemyState other = (EnemyState)obj;

            if (!other.Position.Equals(Position))
                return false;

            for (int i = 0; i < EnemysStatus.Count; i++)
            {
                if (other.EnemysStatus[i] != EnemysStatus[i])
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            string msg = "EnemyState:\nPosition: " + Position.ToString();
            msg += "\nEnemys:\n";
            for (int i = 0; i < EnemysPosition.Count; i++)
            {
                msg += "Position: " + EnemysPosition[i] + " is alive? " + EnemysStatus[i] + ", ";
            }
            return msg;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        private Vector2 Position;
        private List<Vector2> EnemysPosition;
        private List<bool> EnemysStatus;
    }
}
﻿using GPGPC.PCG.LevelCreation;
using System;
using System.Collections.Generic;

namespace GPGPC.PCG.Tests
{
    internal class EnemyTileWorld : TileWorld
    {
        public EnemyTileWorld(int Size)
            : base(Size)
        {
            Interagibles = new Dictionary<string, string>();
        }

        public EnemyTileWorld()
            : base()
        {
            Interagibles = new Dictionary<string, string>();
        }

        public bool AddInteragible(string Simbol, string ActionTakedByHim)
        {
            if (!IsInteractable(Simbol))
            {
                Interagibles.Add(Simbol, ActionTakedByHim);
                return true;
            }

            Console.WriteLine("The interagible already exists!");
            return false;
        }

        /*public bool IsInteragible(string Simbol)
        {
            return Interagibles.ContainsKey(Simbol);
        }*/

        private Dictionary<string, string> Interagibles;
    }
}
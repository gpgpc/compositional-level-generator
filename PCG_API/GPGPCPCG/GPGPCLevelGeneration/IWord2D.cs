﻿using GPGPC.Utils;
using System.Collections.Generic;

namespace GPGPC
{
    /// <summary>
    /// This interfaces represents a two dimensional world.
    /// </summary>
    /// <typeparam name="T">The type of the element that will be used to represent each tile.</typeparam>
    public interface IWorld2D<T>
    {
        /// <summary>
        /// Function that returns a value, given a position on the map.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <returns>A value contained on the map.</returns>
        T GetValue(int X, int Y);

        /// <summary>
        /// This method sets a value into a X and Y position on the map.
        /// </summary>
        /// <param name="X">X position.</param>
        /// <param name="Y">Y position.</param>
        /// <param name="Value">Value to be seted on the map.</param>
        void SetValue(int X, int Y, T Value);

        /// <summary>
        /// The size of a quadratic map.
        /// </summary>
        /// <returns></returns>
        int Length();
        
        /// <summary>
        /// Inserts a simbol as an artifact (feature).
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        bool AddArtifact(T Simbol, Vector2 Position);

        /// <summary>
        /// This method checks if a simbol is a artifact (feature).
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a artifact, false otherwise.</returns>
        bool containsArtifact(T Simbol);

        /// <summary>
        /// This method gets all the position in which a simbol is found.
        /// </summary>
        /// <param name="Simbol">Simbol which the position will be seek.</param>
        /// <returns>A list of positions.</returns>
        List<Vector2> GetArtifactPositions(T Simbol);

        /// <summary>
        /// Inserts a simbol as a interactable.
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        bool AddInteractable(T Simbol);

        /// <summary>
        /// Checks if the simbol is a interactable.
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a interactable, false otherwise.</returns>
        bool IsInteractable(T Simbol);

        /// <summary>
        /// List of the interactables contained on the map.
        /// </summary>
        /// <returns>A list with the interactables on the map.</returns>
        List<T> GetInteractables();

        /// <summary>
        /// Inserts a simbol as a non traversable.
        /// </summary>
        /// <param name="Simbol">Simbol that will be added.</param>
        /// <returns>If the insertion was successful or not.</returns>
        bool AddNonTraversable(T Simbol);

        /// <summary>
        /// Checks if the simbol is a non traversable.
        /// </summary>
        /// <param name="Simbol">Simbol which will be tested.</param>
        /// <returns>True if is a non traversable, false otherwise.</returns>
        bool IsNonTraversable(T Simbol);

        /// <summary>
        /// List of non traversables simbols on the map.
        /// </summary>
        /// <returns>A list containin all non traversables.</returns>
        List<T> GetNonTraversables();

        /// <summary>
        /// Dictionary that maps a interactable to a simbol which will be placed below it.
        /// </summary>
        Dictionary<T, T> Below
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public class ApproximateQAgent<S, A> : QLearningAgent<S, A>
    {

        public ApproximateQAgent(FeatureExtractor<S, A> Extrator, Func<S, A[]> GetActionsFunc, int NumTraining = 100, float ExplorationRate = 0.5f, 
            float LearningRate = 1f, float Discount = 0.9f)
            : base(GetActionsFunc, NumTraining, ExplorationRate, LearningRate, Discount)
        {
            this.Extrator = Extrator;
            this.Weights = new Dictionary<string, float>();
            this.InitializeWeights();
        }

        // returns the dot product between the weight and the feature vector
        public override float QValue(S State, A Action)
        {
            float sumValues = 0f;
            Dictionary<string, float> features = this.Extrator.GetFeatures(State, Action);
            foreach (var feature in features.Keys)
            {
                float qValue = this.Weights[feature] * features[feature];
                sumValues += qValue;
            }

            return sumValues;
        }

        public override void Update(S State, A Action, S NextState, float Reward)
        {
            Dictionary<string, float> features = this.Extrator.GetFeatures(State, Action);
            float difference = Reward + this.Discount * this.Value(NextState);
            difference -= this.QValue(State, Action);

            foreach (var feature in features.Keys)
            {
                float weight = this.Weights[feature];
                float featureValue = features[feature];
                float newWeight = weight + (this.LearningRate * difference * featureValue);
                this.Weights[feature] = newWeight;
            }
        }

        private void InitializeWeights()
        {
            foreach (var feature in Extrator.AllFeatures())
            {
                this.Weights.Add(feature, 0f);
            }
        }

        public FeatureExtractor<S, A> Extrator { get; set; }

        public Dictionary<string, float> Weights { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public abstract class ValueEstimationAgent<S,A>
    {
        public ValueEstimationAgent(float LearningRate = 1.0f, float ExplorationRate = 0.5f, float Discount = 0.8f, int NumTraining = 100)
        {
            this.LearningRate = LearningRate;
            this.ExplorationRate = ExplorationRate;
            this.Discount = Discount;
            this.NumTraining = NumTraining;
        }

        // returns de Q value given the State and Action pair
        public abstract float QValue(S State, A Action);

        // returns the value of this state under all legal actions
        public abstract float Value(S State);

        // returns the action that should be taken following the current policy
        public abstract A Policy(S State);

        // return a action
        public abstract A Action(S State);

        public float LearningRate { get { return this.alpha; } set { this.alpha = (value >= 0) ? value : 0f; } }
        private float alpha;

        public float ExplorationRate { get { return this.epsilon; } set { this.epsilon = (value >= 0) ? value : 0f; } }
        private float epsilon;

        public float Discount { get; set; }

        public int NumTraining { get; set; }

        public static Random PRNG = new Random();

        //
        public bool FlipCoin(float prob)
        {
            return PRNG.NextDouble() <= prob;
        }
    }
}

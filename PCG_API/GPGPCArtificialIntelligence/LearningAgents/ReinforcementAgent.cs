﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public abstract class ReinforcementAgent<S, A> : ValueEstimationAgent<S, A>
    {

        // Need to add here the to recieves the function that will be used on the LegalActions() method
        public ReinforcementAgent(Func<S, A[]> GetActionsFunc, int NumTraining = 100, float ExplorationRate = 0.5f, float LearningRate = 1f, float Discount = 0.9f)
            : base(LearningRate, ExplorationRate, Discount, NumTraining)
        {
            this.EpisodesSoFar = 0;
            this.AccumTrainRewards = 0.0f;
            this.AccumTestRewards = 0.0f;
            this.GetActionsFunc = GetActionsFunc;
        }

        // updates the weights after a transition on the enviroment
        public abstract void Update(S State, A Action, S NextState, float Reward);

        // returns the legal actions given the state State
        public A[] LegalActions(S State)
        {
            return this.GetActionsFunc(State);
        }

        public void ObserveTransition(S State, A Action, S NextState, float DeltaReward)
        {
            // updates the episode rewards and calls the Update method
            this.Update(State, Action, NextState, DeltaReward);
            this.EpisodeRewards += DeltaReward;
        }

        // called by the enviroment when a new episode of training is starting
        public void StartEpisode()
        {
            this.LastState = default(S);
            this.LastAction = default(A);
            this.EpisodeRewards = 0f;
        }

        // called by the enviroment when the episode of training is done
        public void StopEpisode()
        {
            if (this.EpisodesSoFar < this.NumTraining)
                this.AccumTrainRewards += this.EpisodeRewards;
            else
                this.AccumTestRewards += this.EpisodeRewards;

            this.EpisodesSoFar++;

            if (this.EpisodesSoFar >= this.NumTraining)
            {
                this.ExplorationRate = 0.0f;
                this.LearningRate = 0.0f;
            }
        }

        // called when a action is taken in some state
        public void DoAction(S State, A Action)
        {
            this.LastState = State;
            this.LastAction = Action;
        }

        public bool IsTraining
        {
            get
            {
                return this.EpisodesSoFar <= this.NumTraining;
            }
        }

        public bool IsTesting
        {
            get
            {
                return !this.IsTraining;
            }
        }

        public S LastState { get; set; }

        public A LastAction { get; set; }

        public float EpisodeRewards { get; set; }

        public int EpisodesSoFar { get; set; }

        public float AccumTrainRewards { get; set; }

        public float AccumTestRewards { get; set; }

        public Func<S, A[]> GetActionsFunc { get; set; }
    }
}

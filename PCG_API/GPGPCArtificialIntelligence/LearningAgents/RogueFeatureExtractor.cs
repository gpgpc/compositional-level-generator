﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GPGPC.AI.SearchProblems;
using GPGPC.PCG.LevelCreation;
using GPGPC.AI.LearningAgents;
using GPGPC.Utils;
using System;

namespace GPGPC.AI.LearningAgents
{

    public class RogueFeatureExtrator : FeatureExtractor<RogueState, MoveActions>
    {
        private MoveActions[] Actions = { MoveActions.UP, MoveActions.RIGHT, MoveActions.DOWN, MoveActions.LEFT, MoveActions.STOP };

        public override string[] AllFeatures()
        {
            // guloso
            return new string[] { "bias", "enemies_arround", "eats_food", "go_2_exit", "has_walls_arround" };
        }

        public override Dictionary<string, float> GetFeatures(RogueState State, MoveActions Action)
        {
            Dictionary<string, float> feats = new Dictionary<string, float>();
            Vector2 newPosition = RogueState.NextPosition(Action, State.Position, State.StepSize);
            MoveActions[] actions = { MoveActions.UP, MoveActions.RIGHT, MoveActions.DOWN, MoveActions.LEFT, MoveActions.STOP };
            bool hasWallsArround = false;
            int enemiesArround = 0;
            float currentDistToExit = ManhattanDistance(State.Position, RogueState.Exit);
            float nextDistToExit = ManhattanDistance(newPosition, RogueState.Exit);

            // adds the bias feature
            feats.Add("bias", 1);

            // checks the number of enemies arround the intended position
            State.EnemiesPositions.ForEach((enemy) => { if (ManhattanDistance(newPosition, enemy) <= State.StepSize) enemiesArround++; });
            
            if (nextDistToExit < currentDistToExit) feats.Add("go_2_exit", 0.5f);
            else feats.Add("go_2_exit", -0.5f);

            // checks the number of walls arround the intended position
            State.WallsPositions.ForEach((wall) => { if (ManhattanDistance(newPosition, wall) <= State.StepSize) hasWallsArround = true; });

            // ----------------------
            feats.Add("has_walls_arround", hasWallsArround ? 1f : 0);

            // divide all values by 10
            List<string> keys = new List<string>(feats.Keys);
            foreach (var key in keys)
            {
                feats[key] /= 10f;
                //Debug.Log("feat: " + key + " value: " + feats[key]);
            }

            return feats;
        }

        public int Search(Vector2 Initial, Vector2 Exit, List<Vector2> Enemies, List<Vector2> Walls)
        {
            // sets the rogue tile map
            //TileWorld map = null;
            TileWorld map = CreateRogueTileMap(Initial, Exit, Enemies, Walls);
            if (map == null) return int.MaxValue;
            //Debug.Log("ROlou");
            // creates the AI problem
            Vector2 goalPosition = map.GetArtifactPositionAtRandom("GS");
            Vector2 initialPosition = map.GetArtifactPositionAtRandom("IS");
            //Debug.Log("Initial: " + initialPosition);
            //Debug.Log("Target: " + goalPosition);
            State<Vector2> goal = new State<Vector2>(goalPosition);
            TileWordProblem pathProblem = new TileWordProblem();
            pathProblem.SetWorldRep(map);
            pathProblem.SetInicialState(initialPosition);
            pathProblem.SetGoal(goalPosition);
            List<State<Vector2>> path = null;
            path = Searchs<Vector2>.GraphSearch(pathProblem, null, goal);
            // returns the path's cost
            /*
            int cost = 0;
            foreach (var state in path)
            {
                int stepCost = 1;
                Vector2 pos = state.GetValue();
                string simbol = map.GetValue((int) pos.X, (int) pos.Y);
                if (simbol == "WW" || simbol == "W1" || simbol == "W2")
                    stepCost *= 3;
                cost += stepCost;
            }
            return cost == 0 ? int.MaxValue : cost;
            */
            //Debug.Log("cost: " + (path == null ? "aaaf" : ""+path.Count));
            return path == null ? int.MaxValue : path.Count;
        }

        private static TileWorld CreateRogueTileMap(Vector2 Initial, Vector2 Exit, List<Vector2> Enemies, List<Vector2> Walls)
        {
            TileWorld map = new TileWorld(10);
            // initial state
            int x = (int) Math.Max(Math.Abs(Math.Round(Initial.X / 32f)) - 1, 0);
            int y = (int) Math.Max(Math.Abs(Math.Round(Initial.Y / 32f)) - 1, 0);
            // -----------------------------------------------
            if (x > 9 || y > 9) return null;
            //Debug.Log("x: " + Initial.x + " / 32");
            //Debug.Log("y: " + Initial.y + " / 32");
            Vector2 pos = new Vector2(x, y);
            //Debug.Log("initial: " + pos);
            map.AddArtifact("IS", pos);
            map.SetValue(x, y, "IS");
            // goal state
            x = (int) Math.Max(Math.Abs(Math.Round(Exit.X / 32f)) - 1, 0);
            y = (int) Math.Max(Math.Abs(Math.Round(Exit.Y / 32f)) - 1, 0);
            // -----------------------------------------------
            if (x > 9 || y > 9) return null;
            //Debug.Log("x: " + Exit.x + " / 32");
            //Debug.Log("y: " + Exit.y + " / 32");
            //pos.X = x;
            //pos.Y = y;
            //Debug.Log("goal: " + pos);
            map.AddArtifact("GS", new Vector2(x, y));
            map.SetValue(x, y, "GS");

            if (Enemies.Count > 0)
            {
                // enemies
                foreach (var enemy in Enemies)
                {
                    x = (int) Math.Max(Math.Abs(Math.Round(enemy.X / 32f)) - 1, 0);
                    y = (int) Math.Max(Math.Abs(Math.Round(enemy.Y / 32f)) - 1, 0);
                    //pos.X = x;
                    //pos.Y = y;
                    // -----------------------------------------------
                    if (x > 9 || y > 9) return null;
                    //Debug.Log("x: " + enemy.x + " / 32");
                    //Debug.Log("y: " + enemy.y + " / 32");
                    //Debug.Log("enemy: " + pos);
                    map.SetValue(x, y, "EE");
                    map.AddArtifact("EE", new Vector2(x, y));
                }
            }

            if (Walls.Count > 0)
            {
                // walls
                foreach (var wall in Walls)
                {
                    x = (int)Math.Max(Math.Abs(Math.Round(wall.X / 32f)) - 1, 0);
                    y = (int)Math.Max(Math.Abs(Math.Round(wall.Y / 32f)) - 1, 0);
                    //pos.X = x;
                    //pos.Y = y;
                    // -----------------------------------------------
                    if (x > 9 || y > 9) return null;
                    //Debug.Log("x: " + wall.x + " / 32");
                    //Debug.Log("y: " + wall.y + " / 32");
                    //Debug.Log("wall: " + pos);
                    map.SetValue(x, y, "O1");
                    map.AddArtifact("O1", new Vector2(x, y));
                }
            }

            // non traversables
            map.AddNonTraversable("EE");
            map.AddNonTraversable("O1");
            return map;
        }

        public int RogueBFS(Vector2 Initial, Vector2 Exit, List<Vector2> Enemies, List<Vector2> Walls)
        {
            float maxDiff = 100f;
            // creates the initial state
            Queue<State<Vector2>> fringe = new Queue<State<Vector2>>();
            fringe.Enqueue(new State<Vector2>(Initial));
            // a list of visited States
            List<Vector2> closed = new List<Vector2>();
            // loops until the fringe is empty or finds the exit
            while (fringe.Count > 0)
            {
                State<Vector2> current = fringe.Dequeue();
                //Debug.Log("current: " + current.GetValue());
                //Console.WriteLine("current: " + current.GetValue());
                // checks if its the exit position
                if (IsCloseTo(current.GetValue(), Exit, maxDiff))
                {
                    //Debug.Log("achooou --'");
                    //Console.WriteLine("achooou");
                    // returns the cost
                    return BackTrack(current);
                }
                // checks if the position was already visited
                foreach (var visited in closed)
                {
                    if (IsCloseTo(visited, current.GetValue(), maxDiff))
                        continue;
                }
                // if was not, adds it to the closed list
                closed.Add(current.GetValue());
                //Console.WriteLine("expandindo");
                // expands all possible actions
                foreach (MoveActions action in this.Actions)
                {
                    Vector2 nextPosition = RogueState.NextPosition(action, current.GetValue(), 32f);
                    // checks if the position is avaliable
                    foreach (var enemy in Enemies)
                    {
                        if (IsCloseTo(enemy, nextPosition, maxDiff))
                            continue;
                    }

                    foreach (var wall in Walls)
                    {
                        if (IsCloseTo(wall, nextPosition, maxDiff))
                            continue;
                    }
                    // if it is adds to the fringe
                    fringe.Enqueue(new State<Vector2>(nextPosition, current));
                }
            }
            return int.MaxValue;
        }

        public bool IsCloseTo(Vector2 A, Vector2 B, float Dist = 0.5f)
        {
            if ((Math.Abs(A.X - B.X)) > Dist || (Math.Abs(A.Y - B.Y)) > Dist)
            {
                return false;
            }
            return true;
        }

        public static int BackTrack(State<Vector2> Goal)
        {
            int counter = 0;
            //path.Add(Goal);
            // backtracks from the goal state until the root (StateNode/InitialState)
            while (Goal.GetParent() != null)
            {
                Goal = Goal.GetParent();
                counter++;
            }
            return counter;
        }

        public static float ManhattanDistance(Vector2 A, Vector2 B)
        {
            return Math.Abs(A.X - B.X) + Math.Abs(A.Y - B.Y);
        }
    }
}


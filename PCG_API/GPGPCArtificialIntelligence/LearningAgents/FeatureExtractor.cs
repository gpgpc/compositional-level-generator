﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public abstract class FeatureExtractor<S, A>
    {
        public abstract string[] AllFeatures();

        public abstract Dictionary<string, float> GetFeatures(S State, A Action);

    }
}

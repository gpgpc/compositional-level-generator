﻿using GPGPC.Utils;
using GPGPC.AI.LearningAgents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public enum MoveActions
    {
        UP = 0,
        RIGHT = 1,
        DOWN = 2,
        LEFT = 3,
        STOP = 4
    }

    public class RogueState
    {
        public static ApproximateQAgent<RogueState, MoveActions> InstantiateAgent(
            int TrainingEpisodes, float ExplorationRate, float LearningRate, float DiscountRate)
        {
            RogueFeatureExtrator extractor = new RogueFeatureExtrator();
            ApproximateQAgent<RogueState, MoveActions> theLearner;
            //theLearner = new ApproximateQAgent<RogueState, MoveActions>(extractor, null, 1000, 0.03f, 0.02f, 1f);
            theLearner = new ApproximateQAgent<RogueState, MoveActions>(extractor, null, TrainingEpisodes,
                ExplorationRate, LearningRate, DiscountRate);
            return theLearner;
        }

        public RogueState()
        {
            this.EnemiesPositions = new List<Vector2>();
            this.FoodsPositions = new List<Vector2>();
            this.WallsPositions = new List<Vector2>();
            this.HolesPositions = new List<Vector2>();
            this.EnemiesWillMove = false;
            this.StepSize = 32f;
        }

        public static Vector2 NextPosition(MoveActions action, Vector2 Position, float StepSize)
        {
            switch (action)
            {
                case MoveActions.UP:
                    Position.Y += StepSize;
                    return Position;
                case MoveActions.RIGHT:
                    Position.X += StepSize;
                    return Position;
                case MoveActions.DOWN:
                    Position.Y -= StepSize;
                    return Position;
                case MoveActions.LEFT:
                    Position.X -= StepSize;
                    return Position;
                default:
                    return Position;
            }
        }

        public RogueState Clone()
        {
            RogueState clone = new RogueState();
            clone.Position = this.Position;
            clone.EnemiesPositions.AddRange(this.EnemiesPositions);
            clone.FoodsPositions.AddRange(this.FoodsPositions);
            clone.WallsPositions.AddRange(this.WallsPositions);
            clone.HolesPositions.AddRange(this.HolesPositions);
            clone.StepSize = this.StepSize;
            clone.EnemiesWillMove = this.EnemiesWillMove;
            return clone;
        }

        public override string ToString()
        {
            string txt = "";
            txt += "Position: " + this.Position;
            txt += "\nExit: " + Exit;
            txt += "\nEnemies: " + this.EnemiesPositions;
            txt += "\nFoods: " + this.FoodsPositions;
            return txt;
        }

        public override bool Equals(object obj)
        {
            RogueState other = (RogueState)obj;
            if (!other.Position.Equals((this.Position)))
                return false;

            if (!other.EnemiesPositions.Equals(this.EnemiesPositions))
                return false;
            
            if (!other.FoodsPositions.Equals(this.FoodsPositions))
                return false;

            if (!other.WallsPositions.Equals(this.WallsPositions))
                return false;

            if (!other.HolesPositions.Equals(this.HolesPositions))
                return false;

            if (!other.EnemiesWillMove != this.EnemiesWillMove)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Vector2 Exit;
        public Vector2 Position { get; set; }
        public List<Vector2> EnemiesPositions { get; set; }
        public List<Vector2> FoodsPositions { get; set; }
        public List<Vector2> WallsPositions { get; set; }
        public List<Vector2> HolesPositions { get; set; }
        public float StepSize { get; set; }
        public bool EnemiesWillMove { get; set; }


    }
}

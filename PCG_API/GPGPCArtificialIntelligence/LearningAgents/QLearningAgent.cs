﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPGPC.AI.LearningAgents
{
    public class StateActionPair<S,A>
    {
        public StateActionPair(S State, A Action)
        {
            this.State = State;
            this.Action = Action;
        }

        public S State
        {
            get;
            set;
        }

        public A Action
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            StateActionPair<S,A> other = (StateActionPair<S,A>) obj;
            if (!other.State.Equals(this.State))
                return false;
            if (!other.Action.Equals(this.Action))
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    public class QLearningAgent<S, A> : ReinforcementAgent<S, A>
    {
        public QLearningAgent(Func<S, A[]> GetActionsFunc = null, int NumTraining = 100, float ExplorationRate = 0.5f, float LearningRate = 0.7f, float Discount = 0.9f)
            : base(GetActionsFunc, NumTraining, ExplorationRate, LearningRate, Discount)
        {
            this.Values = new Dictionary<StateActionPair<S,A>, float>();

        }

        public override void Update(S State, A Action, S NextState, float Reward)
        {
            float maxQ = this.Value(NextState);
            float sample = Reward + this.Discount * maxQ;
            float newValue = (1 - this.LearningRate) * this.QValue(State, Action);
            newValue += this.LearningRate * sample;

            StateActionPair<S, A> key = new StateActionPair<S, A>(State, Action);
            if (!this.Values.ContainsKey(key))
                this.Values.Add(key, 0f);

            this.Values[key] = newValue;
        }

        public override float QValue(S State, A Action)
        {
            StateActionPair<S, A> key = new StateActionPair<S, A>(State, Action);
            if (!this.Values.ContainsKey(key))
                this.Values.Add(key, 0f);
            return this.Values[key];
        }

        public override float Value(S State)
        {
            return this.ComputeValueFromQValues(State);
        }

        public override A Policy(S State)
        {
            return this.ComputeActionFromQValues(State);
        }

        // computes the action to take on the current state
        // it has a probability Epsilon to take a random action 
        public override A Action(S State)
        {
            A[] legalActions = this.LegalActions(State);
            A action = default(A);
            if (legalActions.Length < 1)
                return action;

            bool takeRandomAction = this.FlipCoin(this.ExplorationRate);

            if (takeRandomAction)
                action = legalActions[PRNG.Next(0, legalActions.Length)];
            else
                action = this.Policy(State);

            return action;
        }

        // returns the max action over the legal actions for the State. In case of not having legal actions, return 0
        private float ComputeValueFromQValues(S State)
        {
            A[] legalActions = this.LegalActions(State);
            if (legalActions.Length < 1)
                return 0f;

            float maxValue = float.MinValue;
            foreach (var action in legalActions)
            {
                float value = this.QValue(State, action);
                if (value > maxValue)
                {
                    maxValue = value;
                }  // under test
                else if (value == maxValue)
                {
                    if (this.FlipCoin(0.5f))
                        continue;
                    maxValue = value;
                }
            }

            return maxValue;
        }

        // returns the best action to take on the current state. If there are no legal actions should return no action
        private A ComputeActionFromQValues(S State)
        {
            A bestAction = default(A);
            float maxValue = float.MinValue;

            foreach (var action in this.LegalActions(State))
            {
                float value = this.QValue(State, action);
                if (value > maxValue)
                {
                    bestAction = action;
                    maxValue = value;
                }  // under test
                else if (value == maxValue)
                {
                    if (this.FlipCoin(0.5f))
                        continue;
                    bestAction = action;
                    maxValue = value;
                }
            }

            return bestAction;
        }

        public Dictionary<StateActionPair<S, A>, float> Values { get; set; }
    }
}

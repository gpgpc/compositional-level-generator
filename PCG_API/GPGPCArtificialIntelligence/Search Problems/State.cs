﻿namespace GPGPC.AI.SearchProblems
{
    public class State<T>
    {
        /// <summary>
        /// Basci constructor, initializes all atributes with the
        /// default configurations.
        /// </summary>
        public State()
        {
            this.Value = default(T);
            this.Parent = null;
            this.Cost = 0;
            this.Action = "";
        }

        /// <summary>
        /// Constructor which initializes the state with a value and default parent,
        /// action and cost.
        /// </summary>
        /// <param name="Value">Value contained on this state.</param>
        public State(T Value)
            : this(Value, null)
        { }

        /// <summary>
        /// Constructor which initializes the state with a value and parent
        /// and default actions and cost.
        /// </summary>
        /// <param name="Value">Value contained on this state.</param>
        /// <param name="Parent">Parent state of this one.</param>
        public State(T Value, State<T> Parent)
        {
            this.Value = Value;
            this.Parent = Parent;
        }

        /// <summary>
        /// This method gets the cost taked to reach this state.
        /// </summary>
        /// <returns>The cost taked to reach this state.</returns>
        public double GetCost()
        {
            return Cost;
        }

        /// <summary>
        /// This method sets the cost to reach this state.
        /// </summary>
        /// <param name="Cost">Cost to reach this state.</param>
        public void SetCost(double Cost)
        {
            this.Cost = Cost;
        }

        /// <summary>
        /// This method gets the value contained in this state.
        /// </summary>
        /// <returns>The value contained in this state.</returns>
        public T GetValue()
        {
            return Value;
        }

        /// <summary>
        /// Sets the value contained in this state.
        /// </summary>
        /// <param name="Value">The value contained in this state.</param>
        public void SetValue(T Value)
        {
            this.Value = Value;
        }

        /// <summary>
        /// This method gets the parent state of this one.
        /// </summary>
        /// <returns>The parent state of this one.</returns>
        public State<T> GetParent()
        {
            return Parent;
        }

        /// <summary>
        /// Sets the parent state of this state.
        /// </summary>
        /// <param name="Parent">The parent state of this state.</param>
        public void SetParent(State<T> Parent)
        {
            this.Parent = Parent;
        }

        /// <summary>
        /// This method gets the action taked to get in this state.
        /// </summary>
        /// <returns>The action taked to get in this state.</returns>
        public string GetAction()
        {
            return Action;
        }

        /// <summary>
        /// Sets the actions taked to get in this state.
        /// </summary>
        /// <param name="Action">The acion taked to get in this state.</param>
        public void SetAction(string Action)
        {
            this.Action = Action;
        }

        public bool Equals(T obj)
        {
            //T other = (T) obj;
            return Value.Equals(obj);
        }

        /// <summary>
        /// The acion taked to get in this state.
        /// </summary>
        private string Action;

        /// <summary>
        /// The true cost to reach this state.
        /// </summary>
        private double Cost;

        /// <summary>
        /// The value which represents this state.
        /// </summary>
        private T Value;

        /// <summary>
        /// Parent of this state. 
        /// </summary>
        private State<T> Parent;
    }
}
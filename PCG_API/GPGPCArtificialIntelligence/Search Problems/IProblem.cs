﻿using System.Collections.Generic;

namespace GPGPC.AI.SearchProblems
{
    /// <summary>
    /// This interfact defines a problem, which is defined by a generic parametrer
    /// representing the value contained on the states.
    /// </summary>
    /// <typeparam name="T">Type of a state value.</typeparam>
    public interface IProblem<T>
    {
        /// <summary>
        /// This method returns the initial state of a problem.
        /// </summary>
        /// <returns></returns>
        State<T> GetInitialState();

        /// <summary>
        /// This method sets the initial state.
        /// </summary>
        /// <param name="InitState">Initial state of the problem.</param>
        void SetInicialState(T InitState);

        /// <summary>
        /// This function is responsible for expanding a state into successors triples.
        /// It should map the current state into a list of successors, the acion taked
        /// to get there and its cost.
        /// </summary>
        /// <param name="Current"></param>
        /// <returns></returns>
        List<Successor<T>> Expand(State<T> Current);

        /// <summary>
        /// This method checks if it is or not the goal state.
        /// </summary>
        /// <param name="Current">State the will be checked.</param>
        /// <returns>If is or not a goal state.</returns>
        bool IsGoalState(State<T> Current);

        /// <summary>
        /// This function calculates the heuristic value from a state.
        /// </summary>
        /// <param name="Current">The current state.</param>
        /// <param name="Goal">The goal state of the search.</param>
        /// <returns>The heuristic value of the current state.</returns>
        double Heuristic(State<T> Current, State<T> Goal);
    }
}
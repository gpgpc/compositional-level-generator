﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GPGPC.AI.SearchProblems
{
    /// <summary>
    /// This struct represents a successor triple, whic contains the successor state,
    /// action taked to get there and the cost of that action.
    /// </summary>
    /// <typeparam name="T">Type of the state.</typeparam>
    public struct Successor<T>
    {
        public T state;
        public string action;
        public double cost;
    }

    /// <summary>
    /// This is a class used to search a given problem.
    /// </summary>
    /// <typeparam name="T">Type of the state value.</typeparam>
    public class Searchs<T>
    {
        /// <summary>
        /// This method performs a graph search, given a problem. It uses a strategy, for the fringe,
        /// allowing to define which search algorithm it will be used. By defaults, it uses the A*
        /// search algorithm.
        /// </summary>
        /// <param name="Problem">Problem representation where the search will be executed.</param>
        /// <param name="FringeStrategy">Strategy used on the fringe controll.</param>
        /// <param name="Goal">State which the search is seeking.</param>
        /// <returns></returns>
        public static List<State<T>> GraphSearch(IProblem<T> Problem, Strategy FringeStrategy, State<T> Goal)
        {
            FringeControl = null;
            if (FringeStrategy != null)
                FringeControl = FringeStrategy;

            NodesExpandeds = 0;
            // A fringe with the heuristic as a Key and a List of States with the same heuristic
            SortedList<double, List<State<T>>> fringe = new SortedList<double, List<State<T>>>();
            // a list of visited States
            List<T> closed = new List<T>();
            // the initial and goal states provided by the Problem
            InitialState = Problem.GetInitialState();
            GoalState = Goal;

            // calculates the initial fitness and adds to the fringe
            double h = 0;
            int cost = 0;
            fringe.Add(h + cost, new List<State<T>>());
            fringe[h].Add(InitialState);
            
            Console.WriteLine("Searching ...");
            //int i = 0;
            //while (i++ < 10)
            while (fringe.Count > 0)
            {
                if (NodesExpandeds % 1000 == 0)
                {
                    Console.WriteLine("Nodes expandeds: {0}", NodesExpandeds);
                }
                // pops the first element on the fringe, build following some strategy
                // then, in case of tiebreak it decides by random
                List<State<T>> contenders = fringe.ElementAt(0).Value;
                int choosen = Rand.Next(contenders.Count);
                State<T> current = contenders[choosen];

                // if its a goal state, returns the path to the root
                if (Problem.IsGoalState(current))
                    return BackTrack(current);

                // if it has only one contender with this heuristic, do not have any more lol
                // other wise removes one of the contenders with the lower heuristc
                if (contenders.Count <= 1)
                    fringe.RemoveAt(0);
                else
                    contenders.RemoveAt(choosen);

                // if a node is on the closed set, it is ignored
                if (!closed.Contains(current.GetValue()))
                {
                    closed.Add(current.GetValue());
                    List<Successor<T>> successors = Problem.Expand(current);
                    NodesExpandeds++;

                    foreach (Successor<T> successorTriple in successors)
                    {
                        State<T> successor = new State<T>();
                        successor.SetValue(successorTriple.state);
                        successor.SetAction(successorTriple.action);
                        successor.SetCost(current.GetCost() + successorTriple.cost);
                        successor.SetParent(current);

                        h = Searchs<T>.FringeStrategy(Problem, current, successor);
                        // insert the current successor on the fringe
                        if (!fringe.ContainsKey(h))
                            fringe.Add(h, new List<State<T>>());
                        fringe[h].Add(successor);
                    }
                }
            }
            // if the fringe gets empty (no goal state was found) it return failure
            Console.WriteLine("Fail after {0} nodes expandeds =( ", NodesExpandeds);
            return null;
        }

        /// <summary>
        /// Function that starts at a goal state and back tracks until the initial
        /// state, constructing the path containing the problem's solution.
        /// </summary>
        /// <param name="Goal">State where the solution of the problem is found.</param>
        /// <returns>A list of states from initial to goal state.</returns>
        public static List<State<T>> BackTrack(State<T> Goal)
        {
            // a queue of the path from Start Node to the Goal State
            List<State<T>> path = new List<State<T>>();
            path.Add(Goal);
            // backtracks from the goal state until the root (StateNode/InitialState)
            while (Goal.GetParent() != null)
            {
                Goal = Goal.GetParent();
                path.Insert(0, Goal);
            }
            Console.WriteLine(ShowSearchInfo(path));
            return path;
        }

        /// <summary>
        /// Fringe strategy used for the A* search. It sets the cost as the
        /// current cost plus 1, which is the cost of all actions on this
        /// environmen and returns the state fitness, which is f(x) = g(x) + h(x).
        /// The heuristic value plus the current cost.
        /// </summary>
        /// <param name="Problem"></param>
        /// <param name="Current"></param>
        /// <param name="Successor"></param>
        /// <returns>The fitness of from current to successor state.</returns>
        public static double FringeStrategy(IProblem<T> Problem, State<T> Current, State<T> Successor)
        {
            if (FringeControl != null)
                return FringeControl(Problem, Current, Successor);

            Successor.SetCost(Current.GetCost() + 1);
            double h = Problem.Heuristic(Current, Searchs<T>.GoalState) + Successor.GetCost();
            Successor.SetParent(Current);
            return h;
        }

        /// <summary>
        /// This method constructs informations about the search.
        /// </summary>
        /// <param name="Path"></param>
        /// <returns>Informations about the search.</returns>
        private static string ShowSearchInfo(List<State<T>> Path)
        {
            string info = "";
            info += "Total nodes expandeds: " + NodesExpandeds;
            info += "\nTotal steps until the goal: " + Path.Count;
            info += "\nBenchmark: " + "TO-DO";
            info += "\n=) \n";
            return info;
        }

        /// <summary>
        /// The controll of the strategy used on the search process.
        /// </summary>
        public static Strategy FringeControl = null;

        /// <summary>
        /// This method allows the use of a strategy for a fringe controll. 
        /// </summary>
        /// <param name="Problem"></param>
        /// <param name="Current"></param>
        /// <param name="Successor"></param>
        /// <returns></returns>
        public delegate double Strategy(IProblem<T> Problem, State<T> Current, State<T> Successor);

        /// <summary>
        /// Number of expanded nodess.
        /// </summary>
        private static int NodesExpandeds;
        
        /// <summary>
        /// Pseudo Random Number Generator.
        /// </summary>
        private static Random Rand = new Random();

        /// <summary>
        /// States that represents the initial and goal state.
        /// </summary>
        private static State<T> InitialState, GoalState;
    }
}